package ee.parkingspot.parkingapi.service;

import ee.parkingspot.parkingapi.domain.ParkingEvent;
import ee.parkingspot.parkingapi.repository.ParkingEventRepository;
import ee.parkingspot.parkingapi.service.dto.ParkingEventDTO;
import ee.parkingspot.parkingapi.service.mapper.ParkingEventMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing ParkingEvent.
 */
@Service
@Transactional
public class ParkingEventService {

    private final Logger log = LoggerFactory.getLogger(ParkingEventService.class);

    private final ParkingEventRepository parkingEventRepository;

    private final ParkingEventMapper parkingEventMapper;

    public ParkingEventService(ParkingEventRepository parkingEventRepository, ParkingEventMapper parkingEventMapper) {
        this.parkingEventRepository = parkingEventRepository;
        this.parkingEventMapper = parkingEventMapper;
    }

    /**
     * Save a parkingEvent.
     *
     * @param parkingEventDTO the entity to save
     * @return the persisted entity
     */
    public ParkingEventDTO save(ParkingEventDTO parkingEventDTO) {
        log.debug("Request to save ParkingEvent : {}", parkingEventDTO);
        ParkingEvent parkingEvent = parkingEventMapper.toEntity(parkingEventDTO);
        parkingEvent = parkingEventRepository.save(parkingEvent);
        return parkingEventMapper.toDto(parkingEvent);
    }

    /**
     *  Get all the parkingEvents.
     *
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<ParkingEventDTO> findAll() {
        log.debug("Request to get all ParkingEvents");
        return parkingEventRepository.findAll().stream()
            .map(parkingEventMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     *  Get one parkingEvent by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public ParkingEventDTO findOne(Long id) {
        log.debug("Request to get ParkingEvent : {}", id);
        ParkingEvent parkingEvent = parkingEventRepository.findOne(id);
        return parkingEventMapper.toDto(parkingEvent);
    }

    /**
     *  Delete the  parkingEvent by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ParkingEvent : {}", id);
        parkingEventRepository.delete(id);
    }
}
