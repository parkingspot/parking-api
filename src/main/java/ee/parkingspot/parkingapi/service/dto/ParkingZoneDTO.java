package ee.parkingspot.parkingapi.service.dto;


import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;
import javax.persistence.Lob;

/**
 * A DTO for the ParkingZone entity.
 */
public class ParkingZoneDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(min = 2)
    private String name;

    @NotNull
    @Size(min = 20)
    @Lob
    private String boundaryPolygon;

    @NotNull
    @DecimalMin(value = "0")
    private BigDecimal parkingFee;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBoundaryPolygon() {
        return boundaryPolygon;
    }

    public void setBoundaryPolygon(String boundaryPolygon) {
        this.boundaryPolygon = boundaryPolygon;
    }

    public BigDecimal getParkingFee() {
        return parkingFee;
    }

    public void setParkingFee(BigDecimal parkingFee) {
        this.parkingFee = parkingFee;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ParkingZoneDTO parkingZoneDTO = (ParkingZoneDTO) o;
        if(parkingZoneDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), parkingZoneDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ParkingZoneDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", boundaryPolygon='" + getBoundaryPolygon() + "'" +
            ", parkingFee='" + getParkingFee() + "'" +
            "}";
    }
}
