package ee.parkingspot.parkingapi.service;

import ee.parkingspot.parkingapi.domain.ParkingZone;
import ee.parkingspot.parkingapi.repository.ParkingZoneRepository;
import ee.parkingspot.parkingapi.service.dto.ParkingZoneDTO;
import ee.parkingspot.parkingapi.service.mapper.ParkingZoneMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing ParkingZone.
 */
@Service
@Transactional
public class ParkingZoneService {

    private final Logger log = LoggerFactory.getLogger(ParkingZoneService.class);

    private final ParkingZoneRepository parkingZoneRepository;

    private final ParkingZoneMapper parkingZoneMapper;

    public ParkingZoneService(ParkingZoneRepository parkingZoneRepository, ParkingZoneMapper parkingZoneMapper) {
        this.parkingZoneRepository = parkingZoneRepository;
        this.parkingZoneMapper = parkingZoneMapper;
    }

    /**
     * Save a parkingZone.
     *
     * @param parkingZoneDTO the entity to save
     * @return the persisted entity
     */
    public ParkingZoneDTO save(ParkingZoneDTO parkingZoneDTO) {
        log.debug("Request to save ParkingZone : {}", parkingZoneDTO);
        ParkingZone parkingZone = parkingZoneMapper.toEntity(parkingZoneDTO);
        parkingZone = parkingZoneRepository.save(parkingZone);
        return parkingZoneMapper.toDto(parkingZone);
    }

    /**
     *  Get all the parkingZones.
     *
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<ParkingZoneDTO> findAll() {
        log.debug("Request to get all ParkingZones");
        return parkingZoneRepository.findAll().stream()
            .map(parkingZoneMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     *  Get one parkingZone by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public ParkingZoneDTO findOne(Long id) {
        log.debug("Request to get ParkingZone : {}", id);
        ParkingZone parkingZone = parkingZoneRepository.findOne(id);
        return parkingZoneMapper.toDto(parkingZone);
    }

    /**
     *  Delete the  parkingZone by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ParkingZone : {}", id);
        parkingZoneRepository.delete(id);
    }
}
