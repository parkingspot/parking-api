package ee.parkingspot.parkingapi.service.mapper;

import ee.parkingspot.parkingapi.domain.*;
import ee.parkingspot.parkingapi.service.dto.ParkingEventDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity ParkingEvent and its DTO ParkingEventDTO.
 */
@Mapper(componentModel = "spring", uses = {ParkingZoneMapper.class})
public interface ParkingEventMapper extends EntityMapper<ParkingEventDTO, ParkingEvent> {

    @Mapping(source = "parkingZone.id", target = "parkingZoneId")
    @Mapping(source = "parkingZone.name", target = "parkingZoneName")
    ParkingEventDTO toDto(ParkingEvent parkingEvent); 

    @Mapping(source = "parkingZoneId", target = "parkingZone")
    ParkingEvent toEntity(ParkingEventDTO parkingEventDTO);

    default ParkingEvent fromId(Long id) {
        if (id == null) {
            return null;
        }
        ParkingEvent parkingEvent = new ParkingEvent();
        parkingEvent.setId(id);
        return parkingEvent;
    }
}
