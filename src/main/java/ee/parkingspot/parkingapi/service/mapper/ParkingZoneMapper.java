package ee.parkingspot.parkingapi.service.mapper;

import ee.parkingspot.parkingapi.domain.*;
import ee.parkingspot.parkingapi.service.dto.ParkingZoneDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity ParkingZone and its DTO ParkingZoneDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ParkingZoneMapper extends EntityMapper<ParkingZoneDTO, ParkingZone> {

    

    

    default ParkingZone fromId(Long id) {
        if (id == null) {
            return null;
        }
        ParkingZone parkingZone = new ParkingZone();
        parkingZone.setId(id);
        return parkingZone;
    }
}
