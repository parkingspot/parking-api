package ee.parkingspot.parkingapi.web.rest;

import com.codahale.metrics.annotation.Timed;
import ee.parkingspot.parkingapi.service.ParkingZoneService;
import ee.parkingspot.parkingapi.web.rest.errors.BadRequestAlertException;
import ee.parkingspot.parkingapi.web.rest.util.HeaderUtil;
import ee.parkingspot.parkingapi.service.dto.ParkingZoneDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ParkingZone.
 */
@RestController
@RequestMapping("/api")
public class ParkingZoneResource {

    private final Logger log = LoggerFactory.getLogger(ParkingZoneResource.class);

    private static final String ENTITY_NAME = "parkingZone";

    private final ParkingZoneService parkingZoneService;

    public ParkingZoneResource(ParkingZoneService parkingZoneService) {
        this.parkingZoneService = parkingZoneService;
    }

    /**
     * POST  /parking-zones : Create a new parkingZone.
     *
     * @param parkingZoneDTO the parkingZoneDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new parkingZoneDTO, or with status 400 (Bad Request) if the parkingZone has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/parking-zones")
    @Timed
    public ResponseEntity<ParkingZoneDTO> createParkingZone(@Valid @RequestBody ParkingZoneDTO parkingZoneDTO) throws URISyntaxException {
        log.debug("REST request to save ParkingZone : {}", parkingZoneDTO);
        if (parkingZoneDTO.getId() != null) {
            throw new BadRequestAlertException("A new parkingZone cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ParkingZoneDTO result = parkingZoneService.save(parkingZoneDTO);
        return ResponseEntity.created(new URI("/api/parking-zones/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /parking-zones : Updates an existing parkingZone.
     *
     * @param parkingZoneDTO the parkingZoneDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated parkingZoneDTO,
     * or with status 400 (Bad Request) if the parkingZoneDTO is not valid,
     * or with status 500 (Internal Server Error) if the parkingZoneDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/parking-zones")
    @Timed
    public ResponseEntity<ParkingZoneDTO> updateParkingZone(@Valid @RequestBody ParkingZoneDTO parkingZoneDTO) throws URISyntaxException {
        log.debug("REST request to update ParkingZone : {}", parkingZoneDTO);
        if (parkingZoneDTO.getId() == null) {
            return createParkingZone(parkingZoneDTO);
        }
        ParkingZoneDTO result = parkingZoneService.save(parkingZoneDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, parkingZoneDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /parking-zones : get all the parkingZones.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of parkingZones in body
     */
    @GetMapping("/parking-zones")
    @Timed
    public List<ParkingZoneDTO> getAllParkingZones() {
        log.debug("REST request to get all ParkingZones");
        return parkingZoneService.findAll();
        }

    /**
     * GET  /parking-zones/:id : get the "id" parkingZone.
     *
     * @param id the id of the parkingZoneDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the parkingZoneDTO, or with status 404 (Not Found)
     */
    @GetMapping("/parking-zones/{id}")
    @Timed
    public ResponseEntity<ParkingZoneDTO> getParkingZone(@PathVariable Long id) {
        log.debug("REST request to get ParkingZone : {}", id);
        ParkingZoneDTO parkingZoneDTO = parkingZoneService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(parkingZoneDTO));
    }

    /**
     * DELETE  /parking-zones/:id : delete the "id" parkingZone.
     *
     * @param id the id of the parkingZoneDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/parking-zones/{id}")
    @Timed
    public ResponseEntity<Void> deleteParkingZone(@PathVariable Long id) {
        log.debug("REST request to delete ParkingZone : {}", id);
        parkingZoneService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
