/**
 * View Models used by Spring MVC REST controllers.
 */
package ee.parkingspot.parkingapi.web.rest.vm;
