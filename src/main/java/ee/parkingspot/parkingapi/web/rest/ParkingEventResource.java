package ee.parkingspot.parkingapi.web.rest;

import com.codahale.metrics.annotation.Timed;
import ee.parkingspot.parkingapi.service.ParkingEventService;
import ee.parkingspot.parkingapi.web.rest.errors.BadRequestAlertException;
import ee.parkingspot.parkingapi.web.rest.util.HeaderUtil;
import ee.parkingspot.parkingapi.service.dto.ParkingEventDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ParkingEvent.
 */
@RestController
@RequestMapping("/api")
public class ParkingEventResource {

    private final Logger log = LoggerFactory.getLogger(ParkingEventResource.class);

    private static final String ENTITY_NAME = "parkingEvent";

    private final ParkingEventService parkingEventService;

    public ParkingEventResource(ParkingEventService parkingEventService) {
        this.parkingEventService = parkingEventService;
    }

    /**
     * POST  /parking-events : Create a new parkingEvent.
     *
     * @param parkingEventDTO the parkingEventDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new parkingEventDTO, or with status 400 (Bad Request) if the parkingEvent has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/parking-events")
    @Timed
    public ResponseEntity<ParkingEventDTO> createParkingEvent(@Valid @RequestBody ParkingEventDTO parkingEventDTO) throws URISyntaxException {
        log.debug("REST request to save ParkingEvent : {}", parkingEventDTO);
        if (parkingEventDTO.getId() != null) {
            throw new BadRequestAlertException("A new parkingEvent cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ParkingEventDTO result = parkingEventService.save(parkingEventDTO);
        return ResponseEntity.created(new URI("/api/parking-events/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /parking-events : Updates an existing parkingEvent.
     *
     * @param parkingEventDTO the parkingEventDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated parkingEventDTO,
     * or with status 400 (Bad Request) if the parkingEventDTO is not valid,
     * or with status 500 (Internal Server Error) if the parkingEventDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/parking-events")
    @Timed
    public ResponseEntity<ParkingEventDTO> updateParkingEvent(@Valid @RequestBody ParkingEventDTO parkingEventDTO) throws URISyntaxException {
        log.debug("REST request to update ParkingEvent : {}", parkingEventDTO);
        if (parkingEventDTO.getId() == null) {
            return createParkingEvent(parkingEventDTO);
        }
        ParkingEventDTO result = parkingEventService.save(parkingEventDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, parkingEventDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /parking-events : get all the parkingEvents.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of parkingEvents in body
     */
    @GetMapping("/parking-events")
    @Timed
    public List<ParkingEventDTO> getAllParkingEvents() {
        log.debug("REST request to get all ParkingEvents");
        return parkingEventService.findAll();
        }

    /**
     * GET  /parking-events/:id : get the "id" parkingEvent.
     *
     * @param id the id of the parkingEventDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the parkingEventDTO, or with status 404 (Not Found)
     */
    @GetMapping("/parking-events/{id}")
    @Timed
    public ResponseEntity<ParkingEventDTO> getParkingEvent(@PathVariable Long id) {
        log.debug("REST request to get ParkingEvent : {}", id);
        ParkingEventDTO parkingEventDTO = parkingEventService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(parkingEventDTO));
    }

    /**
     * DELETE  /parking-events/:id : delete the "id" parkingEvent.
     *
     * @param id the id of the parkingEventDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/parking-events/{id}")
    @Timed
    public ResponseEntity<Void> deleteParkingEvent(@PathVariable Long id) {
        log.debug("REST request to delete ParkingEvent : {}", id);
        parkingEventService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
