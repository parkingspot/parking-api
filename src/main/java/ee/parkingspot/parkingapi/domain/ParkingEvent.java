package ee.parkingspot.parkingapi.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A ParkingEvent.
 */
@Entity
@Table(name = "parking_event")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ParkingEvent implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "jhi_start", nullable = false)
    private ZonedDateTime start;

    @Column(name = "finish")
    private ZonedDateTime finish;

    @ManyToOne(optional = false)
    @NotNull
    private ParkingZone parkingZone;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getStart() {
        return start;
    }

    public ParkingEvent start(ZonedDateTime start) {
        this.start = start;
        return this;
    }

    public void setStart(ZonedDateTime start) {
        this.start = start;
    }

    public ZonedDateTime getFinish() {
        return finish;
    }

    public ParkingEvent finish(ZonedDateTime finish) {
        this.finish = finish;
        return this;
    }

    public void setFinish(ZonedDateTime finish) {
        this.finish = finish;
    }

    public ParkingZone getParkingZone() {
        return parkingZone;
    }

    public ParkingEvent parkingZone(ParkingZone parkingZone) {
        this.parkingZone = parkingZone;
        return this;
    }

    public void setParkingZone(ParkingZone parkingZone) {
        this.parkingZone = parkingZone;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ParkingEvent parkingEvent = (ParkingEvent) o;
        if (parkingEvent.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), parkingEvent.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ParkingEvent{" +
            "id=" + getId() +
            ", start='" + getStart() + "'" +
            ", finish='" + getFinish() + "'" +
            "}";
    }
}
