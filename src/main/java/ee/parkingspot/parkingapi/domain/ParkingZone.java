package ee.parkingspot.parkingapi.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * A ParkingZone.
 */
@Entity
@Table(name = "parking_zone")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ParkingZone implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Size(min = 2)
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull
    @Size(min = 20)
    @Lob
    @Column(name = "boundary_polygon", nullable = false)
    private String boundaryPolygon;

    @NotNull
    @DecimalMin(value = "0")
    @Column(name = "parking_fee", precision=10, scale=2, nullable = false)
    private BigDecimal parkingFee;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public ParkingZone name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBoundaryPolygon() {
        return boundaryPolygon;
    }

    public ParkingZone boundaryPolygon(String boundaryPolygon) {
        this.boundaryPolygon = boundaryPolygon;
        return this;
    }

    public void setBoundaryPolygon(String boundaryPolygon) {
        this.boundaryPolygon = boundaryPolygon;
    }

    public BigDecimal getParkingFee() {
        return parkingFee;
    }

    public ParkingZone parkingFee(BigDecimal parkingFee) {
        this.parkingFee = parkingFee;
        return this;
    }

    public void setParkingFee(BigDecimal parkingFee) {
        this.parkingFee = parkingFee;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ParkingZone parkingZone = (ParkingZone) o;
        if (parkingZone.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), parkingZone.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ParkingZone{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", boundaryPolygon='" + getBoundaryPolygon() + "'" +
            ", parkingFee='" + getParkingFee() + "'" +
            "}";
    }
}
