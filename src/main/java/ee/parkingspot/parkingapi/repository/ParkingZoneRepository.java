package ee.parkingspot.parkingapi.repository;

import ee.parkingspot.parkingapi.domain.ParkingZone;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the ParkingZone entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ParkingZoneRepository extends JpaRepository<ParkingZone, Long> {

}
