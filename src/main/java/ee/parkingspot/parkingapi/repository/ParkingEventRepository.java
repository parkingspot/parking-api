package ee.parkingspot.parkingapi.repository;

import ee.parkingspot.parkingapi.domain.ParkingEvent;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the ParkingEvent entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ParkingEventRepository extends JpaRepository<ParkingEvent, Long> {

}
