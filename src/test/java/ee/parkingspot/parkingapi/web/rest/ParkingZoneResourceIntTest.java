package ee.parkingspot.parkingapi.web.rest;

import ee.parkingspot.parkingapi.ParkingapiApp;

import ee.parkingspot.parkingapi.domain.ParkingZone;
import ee.parkingspot.parkingapi.repository.ParkingZoneRepository;
import ee.parkingspot.parkingapi.service.ParkingZoneService;
import ee.parkingspot.parkingapi.service.dto.ParkingZoneDTO;
import ee.parkingspot.parkingapi.service.mapper.ParkingZoneMapper;
import ee.parkingspot.parkingapi.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;

import static ee.parkingspot.parkingapi.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ParkingZoneResource REST controller.
 *
 * @see ParkingZoneResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ParkingapiApp.class)
public class ParkingZoneResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_BOUNDARY_POLYGON = "AAAAAAAAAAAAAAAAAAAA";
    private static final String UPDATED_BOUNDARY_POLYGON = "BBBBBBBBBBBBBBBBBBBB";

    private static final BigDecimal DEFAULT_PARKING_FEE = new BigDecimal(0);
    private static final BigDecimal UPDATED_PARKING_FEE = new BigDecimal(1);

    @Autowired
    private ParkingZoneRepository parkingZoneRepository;

    @Autowired
    private ParkingZoneMapper parkingZoneMapper;

    @Autowired
    private ParkingZoneService parkingZoneService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restParkingZoneMockMvc;

    private ParkingZone parkingZone;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ParkingZoneResource parkingZoneResource = new ParkingZoneResource(parkingZoneService);
        this.restParkingZoneMockMvc = MockMvcBuilders.standaloneSetup(parkingZoneResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ParkingZone createEntity(EntityManager em) {
        ParkingZone parkingZone = new ParkingZone()
            .name(DEFAULT_NAME)
            .boundaryPolygon(DEFAULT_BOUNDARY_POLYGON)
            .parkingFee(DEFAULT_PARKING_FEE);
        return parkingZone;
    }

    @Before
    public void initTest() {
        parkingZone = createEntity(em);
    }

    @Test
    @Transactional
    public void createParkingZone() throws Exception {
        int databaseSizeBeforeCreate = parkingZoneRepository.findAll().size();

        // Create the ParkingZone
        ParkingZoneDTO parkingZoneDTO = parkingZoneMapper.toDto(parkingZone);
        restParkingZoneMockMvc.perform(post("/api/parking-zones")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(parkingZoneDTO)))
            .andExpect(status().isCreated());

        // Validate the ParkingZone in the database
        List<ParkingZone> parkingZoneList = parkingZoneRepository.findAll();
        assertThat(parkingZoneList).hasSize(databaseSizeBeforeCreate + 1);
        ParkingZone testParkingZone = parkingZoneList.get(parkingZoneList.size() - 1);
        assertThat(testParkingZone.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testParkingZone.getBoundaryPolygon()).isEqualTo(DEFAULT_BOUNDARY_POLYGON);
        assertThat(testParkingZone.getParkingFee()).isEqualTo(DEFAULT_PARKING_FEE);
    }

    @Test
    @Transactional
    public void createParkingZoneWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = parkingZoneRepository.findAll().size();

        // Create the ParkingZone with an existing ID
        parkingZone.setId(1L);
        ParkingZoneDTO parkingZoneDTO = parkingZoneMapper.toDto(parkingZone);

        // An entity with an existing ID cannot be created, so this API call must fail
        restParkingZoneMockMvc.perform(post("/api/parking-zones")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(parkingZoneDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ParkingZone in the database
        List<ParkingZone> parkingZoneList = parkingZoneRepository.findAll();
        assertThat(parkingZoneList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = parkingZoneRepository.findAll().size();
        // set the field null
        parkingZone.setName(null);

        // Create the ParkingZone, which fails.
        ParkingZoneDTO parkingZoneDTO = parkingZoneMapper.toDto(parkingZone);

        restParkingZoneMockMvc.perform(post("/api/parking-zones")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(parkingZoneDTO)))
            .andExpect(status().isBadRequest());

        List<ParkingZone> parkingZoneList = parkingZoneRepository.findAll();
        assertThat(parkingZoneList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkBoundaryPolygonIsRequired() throws Exception {
        int databaseSizeBeforeTest = parkingZoneRepository.findAll().size();
        // set the field null
        parkingZone.setBoundaryPolygon(null);

        // Create the ParkingZone, which fails.
        ParkingZoneDTO parkingZoneDTO = parkingZoneMapper.toDto(parkingZone);

        restParkingZoneMockMvc.perform(post("/api/parking-zones")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(parkingZoneDTO)))
            .andExpect(status().isBadRequest());

        List<ParkingZone> parkingZoneList = parkingZoneRepository.findAll();
        assertThat(parkingZoneList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkParkingFeeIsRequired() throws Exception {
        int databaseSizeBeforeTest = parkingZoneRepository.findAll().size();
        // set the field null
        parkingZone.setParkingFee(null);

        // Create the ParkingZone, which fails.
        ParkingZoneDTO parkingZoneDTO = parkingZoneMapper.toDto(parkingZone);

        restParkingZoneMockMvc.perform(post("/api/parking-zones")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(parkingZoneDTO)))
            .andExpect(status().isBadRequest());

        List<ParkingZone> parkingZoneList = parkingZoneRepository.findAll();
        assertThat(parkingZoneList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllParkingZones() throws Exception {
        // Initialize the database
        parkingZoneRepository.saveAndFlush(parkingZone);

        // Get all the parkingZoneList
        restParkingZoneMockMvc.perform(get("/api/parking-zones?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(parkingZone.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].boundaryPolygon").value(hasItem(DEFAULT_BOUNDARY_POLYGON.toString())))
            .andExpect(jsonPath("$.[*].parkingFee").value(hasItem(DEFAULT_PARKING_FEE.intValue())));
    }

    @Test
    @Transactional
    public void getParkingZone() throws Exception {
        // Initialize the database
        parkingZoneRepository.saveAndFlush(parkingZone);

        // Get the parkingZone
        restParkingZoneMockMvc.perform(get("/api/parking-zones/{id}", parkingZone.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(parkingZone.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.boundaryPolygon").value(DEFAULT_BOUNDARY_POLYGON.toString()))
            .andExpect(jsonPath("$.parkingFee").value(DEFAULT_PARKING_FEE.intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingParkingZone() throws Exception {
        // Get the parkingZone
        restParkingZoneMockMvc.perform(get("/api/parking-zones/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateParkingZone() throws Exception {
        // Initialize the database
        parkingZoneRepository.saveAndFlush(parkingZone);
        int databaseSizeBeforeUpdate = parkingZoneRepository.findAll().size();

        // Update the parkingZone
        ParkingZone updatedParkingZone = parkingZoneRepository.findOne(parkingZone.getId());
        updatedParkingZone
            .name(UPDATED_NAME)
            .boundaryPolygon(UPDATED_BOUNDARY_POLYGON)
            .parkingFee(UPDATED_PARKING_FEE);
        ParkingZoneDTO parkingZoneDTO = parkingZoneMapper.toDto(updatedParkingZone);

        restParkingZoneMockMvc.perform(put("/api/parking-zones")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(parkingZoneDTO)))
            .andExpect(status().isOk());

        // Validate the ParkingZone in the database
        List<ParkingZone> parkingZoneList = parkingZoneRepository.findAll();
        assertThat(parkingZoneList).hasSize(databaseSizeBeforeUpdate);
        ParkingZone testParkingZone = parkingZoneList.get(parkingZoneList.size() - 1);
        assertThat(testParkingZone.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testParkingZone.getBoundaryPolygon()).isEqualTo(UPDATED_BOUNDARY_POLYGON);
        assertThat(testParkingZone.getParkingFee()).isEqualTo(UPDATED_PARKING_FEE);
    }

    @Test
    @Transactional
    public void updateNonExistingParkingZone() throws Exception {
        int databaseSizeBeforeUpdate = parkingZoneRepository.findAll().size();

        // Create the ParkingZone
        ParkingZoneDTO parkingZoneDTO = parkingZoneMapper.toDto(parkingZone);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restParkingZoneMockMvc.perform(put("/api/parking-zones")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(parkingZoneDTO)))
            .andExpect(status().isCreated());

        // Validate the ParkingZone in the database
        List<ParkingZone> parkingZoneList = parkingZoneRepository.findAll();
        assertThat(parkingZoneList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteParkingZone() throws Exception {
        // Initialize the database
        parkingZoneRepository.saveAndFlush(parkingZone);
        int databaseSizeBeforeDelete = parkingZoneRepository.findAll().size();

        // Get the parkingZone
        restParkingZoneMockMvc.perform(delete("/api/parking-zones/{id}", parkingZone.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ParkingZone> parkingZoneList = parkingZoneRepository.findAll();
        assertThat(parkingZoneList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ParkingZone.class);
        ParkingZone parkingZone1 = new ParkingZone();
        parkingZone1.setId(1L);
        ParkingZone parkingZone2 = new ParkingZone();
        parkingZone2.setId(parkingZone1.getId());
        assertThat(parkingZone1).isEqualTo(parkingZone2);
        parkingZone2.setId(2L);
        assertThat(parkingZone1).isNotEqualTo(parkingZone2);
        parkingZone1.setId(null);
        assertThat(parkingZone1).isNotEqualTo(parkingZone2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ParkingZoneDTO.class);
        ParkingZoneDTO parkingZoneDTO1 = new ParkingZoneDTO();
        parkingZoneDTO1.setId(1L);
        ParkingZoneDTO parkingZoneDTO2 = new ParkingZoneDTO();
        assertThat(parkingZoneDTO1).isNotEqualTo(parkingZoneDTO2);
        parkingZoneDTO2.setId(parkingZoneDTO1.getId());
        assertThat(parkingZoneDTO1).isEqualTo(parkingZoneDTO2);
        parkingZoneDTO2.setId(2L);
        assertThat(parkingZoneDTO1).isNotEqualTo(parkingZoneDTO2);
        parkingZoneDTO1.setId(null);
        assertThat(parkingZoneDTO1).isNotEqualTo(parkingZoneDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(parkingZoneMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(parkingZoneMapper.fromId(null)).isNull();
    }
}
