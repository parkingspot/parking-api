package ee.parkingspot.parkingapi.web.rest;

import ee.parkingspot.parkingapi.ParkingapiApp;

import ee.parkingspot.parkingapi.domain.ParkingEvent;
import ee.parkingspot.parkingapi.domain.ParkingZone;
import ee.parkingspot.parkingapi.repository.ParkingEventRepository;
import ee.parkingspot.parkingapi.service.ParkingEventService;
import ee.parkingspot.parkingapi.service.dto.ParkingEventDTO;
import ee.parkingspot.parkingapi.service.mapper.ParkingEventMapper;
import ee.parkingspot.parkingapi.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static ee.parkingspot.parkingapi.web.rest.TestUtil.sameInstant;
import static ee.parkingspot.parkingapi.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ParkingEventResource REST controller.
 *
 * @see ParkingEventResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ParkingapiApp.class)
public class ParkingEventResourceIntTest {

    private static final ZonedDateTime DEFAULT_START = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_START = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_FINISH = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_FINISH = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    @Autowired
    private ParkingEventRepository parkingEventRepository;

    @Autowired
    private ParkingEventMapper parkingEventMapper;

    @Autowired
    private ParkingEventService parkingEventService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restParkingEventMockMvc;

    private ParkingEvent parkingEvent;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ParkingEventResource parkingEventResource = new ParkingEventResource(parkingEventService);
        this.restParkingEventMockMvc = MockMvcBuilders.standaloneSetup(parkingEventResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ParkingEvent createEntity(EntityManager em) {
        ParkingEvent parkingEvent = new ParkingEvent()
            .start(DEFAULT_START)
            .finish(DEFAULT_FINISH);
        // Add required entity
        ParkingZone parkingZone = ParkingZoneResourceIntTest.createEntity(em);
        em.persist(parkingZone);
        em.flush();
        parkingEvent.setParkingZone(parkingZone);
        return parkingEvent;
    }

    @Before
    public void initTest() {
        parkingEvent = createEntity(em);
    }

    @Test
    @Transactional
    public void createParkingEvent() throws Exception {
        int databaseSizeBeforeCreate = parkingEventRepository.findAll().size();

        // Create the ParkingEvent
        ParkingEventDTO parkingEventDTO = parkingEventMapper.toDto(parkingEvent);
        restParkingEventMockMvc.perform(post("/api/parking-events")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(parkingEventDTO)))
            .andExpect(status().isCreated());

        // Validate the ParkingEvent in the database
        List<ParkingEvent> parkingEventList = parkingEventRepository.findAll();
        assertThat(parkingEventList).hasSize(databaseSizeBeforeCreate + 1);
        ParkingEvent testParkingEvent = parkingEventList.get(parkingEventList.size() - 1);
        assertThat(testParkingEvent.getStart()).isEqualTo(DEFAULT_START);
        assertThat(testParkingEvent.getFinish()).isEqualTo(DEFAULT_FINISH);
    }

    @Test
    @Transactional
    public void createParkingEventWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = parkingEventRepository.findAll().size();

        // Create the ParkingEvent with an existing ID
        parkingEvent.setId(1L);
        ParkingEventDTO parkingEventDTO = parkingEventMapper.toDto(parkingEvent);

        // An entity with an existing ID cannot be created, so this API call must fail
        restParkingEventMockMvc.perform(post("/api/parking-events")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(parkingEventDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ParkingEvent in the database
        List<ParkingEvent> parkingEventList = parkingEventRepository.findAll();
        assertThat(parkingEventList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkStartIsRequired() throws Exception {
        int databaseSizeBeforeTest = parkingEventRepository.findAll().size();
        // set the field null
        parkingEvent.setStart(null);

        // Create the ParkingEvent, which fails.
        ParkingEventDTO parkingEventDTO = parkingEventMapper.toDto(parkingEvent);

        restParkingEventMockMvc.perform(post("/api/parking-events")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(parkingEventDTO)))
            .andExpect(status().isBadRequest());

        List<ParkingEvent> parkingEventList = parkingEventRepository.findAll();
        assertThat(parkingEventList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllParkingEvents() throws Exception {
        // Initialize the database
        parkingEventRepository.saveAndFlush(parkingEvent);

        // Get all the parkingEventList
        restParkingEventMockMvc.perform(get("/api/parking-events?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(parkingEvent.getId().intValue())))
            .andExpect(jsonPath("$.[*].start").value(hasItem(sameInstant(DEFAULT_START))))
            .andExpect(jsonPath("$.[*].finish").value(hasItem(sameInstant(DEFAULT_FINISH))));
    }

    @Test
    @Transactional
    public void getParkingEvent() throws Exception {
        // Initialize the database
        parkingEventRepository.saveAndFlush(parkingEvent);

        // Get the parkingEvent
        restParkingEventMockMvc.perform(get("/api/parking-events/{id}", parkingEvent.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(parkingEvent.getId().intValue()))
            .andExpect(jsonPath("$.start").value(sameInstant(DEFAULT_START)))
            .andExpect(jsonPath("$.finish").value(sameInstant(DEFAULT_FINISH)));
    }

    @Test
    @Transactional
    public void getNonExistingParkingEvent() throws Exception {
        // Get the parkingEvent
        restParkingEventMockMvc.perform(get("/api/parking-events/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateParkingEvent() throws Exception {
        // Initialize the database
        parkingEventRepository.saveAndFlush(parkingEvent);
        int databaseSizeBeforeUpdate = parkingEventRepository.findAll().size();

        // Update the parkingEvent
        ParkingEvent updatedParkingEvent = parkingEventRepository.findOne(parkingEvent.getId());
        updatedParkingEvent
            .start(UPDATED_START)
            .finish(UPDATED_FINISH);
        ParkingEventDTO parkingEventDTO = parkingEventMapper.toDto(updatedParkingEvent);

        restParkingEventMockMvc.perform(put("/api/parking-events")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(parkingEventDTO)))
            .andExpect(status().isOk());

        // Validate the ParkingEvent in the database
        List<ParkingEvent> parkingEventList = parkingEventRepository.findAll();
        assertThat(parkingEventList).hasSize(databaseSizeBeforeUpdate);
        ParkingEvent testParkingEvent = parkingEventList.get(parkingEventList.size() - 1);
        assertThat(testParkingEvent.getStart()).isEqualTo(UPDATED_START);
        assertThat(testParkingEvent.getFinish()).isEqualTo(UPDATED_FINISH);
    }

    @Test
    @Transactional
    public void updateNonExistingParkingEvent() throws Exception {
        int databaseSizeBeforeUpdate = parkingEventRepository.findAll().size();

        // Create the ParkingEvent
        ParkingEventDTO parkingEventDTO = parkingEventMapper.toDto(parkingEvent);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restParkingEventMockMvc.perform(put("/api/parking-events")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(parkingEventDTO)))
            .andExpect(status().isCreated());

        // Validate the ParkingEvent in the database
        List<ParkingEvent> parkingEventList = parkingEventRepository.findAll();
        assertThat(parkingEventList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteParkingEvent() throws Exception {
        // Initialize the database
        parkingEventRepository.saveAndFlush(parkingEvent);
        int databaseSizeBeforeDelete = parkingEventRepository.findAll().size();

        // Get the parkingEvent
        restParkingEventMockMvc.perform(delete("/api/parking-events/{id}", parkingEvent.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ParkingEvent> parkingEventList = parkingEventRepository.findAll();
        assertThat(parkingEventList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ParkingEvent.class);
        ParkingEvent parkingEvent1 = new ParkingEvent();
        parkingEvent1.setId(1L);
        ParkingEvent parkingEvent2 = new ParkingEvent();
        parkingEvent2.setId(parkingEvent1.getId());
        assertThat(parkingEvent1).isEqualTo(parkingEvent2);
        parkingEvent2.setId(2L);
        assertThat(parkingEvent1).isNotEqualTo(parkingEvent2);
        parkingEvent1.setId(null);
        assertThat(parkingEvent1).isNotEqualTo(parkingEvent2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ParkingEventDTO.class);
        ParkingEventDTO parkingEventDTO1 = new ParkingEventDTO();
        parkingEventDTO1.setId(1L);
        ParkingEventDTO parkingEventDTO2 = new ParkingEventDTO();
        assertThat(parkingEventDTO1).isNotEqualTo(parkingEventDTO2);
        parkingEventDTO2.setId(parkingEventDTO1.getId());
        assertThat(parkingEventDTO1).isEqualTo(parkingEventDTO2);
        parkingEventDTO2.setId(2L);
        assertThat(parkingEventDTO1).isNotEqualTo(parkingEventDTO2);
        parkingEventDTO1.setId(null);
        assertThat(parkingEventDTO1).isNotEqualTo(parkingEventDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(parkingEventMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(parkingEventMapper.fromId(null)).isNull();
    }
}
